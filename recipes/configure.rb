# Used to configure the many things of metricbeat
directory '/etc/metricbeat' do
  owner 'root'
  group 'root'
  mode 0o644
  notifies :restart, 'service[metricbeat]', :delayed
end

directory '/etc/metricbeat/modules.d' do
  owner 'root'
  group 'root'
  mode 0o755
  notifies :restart, 'service[metricbeat]', :delayed
end

template '/etc/metricbeat/metricbeat.yml' do
  source 'metricbeat.yml.erb'
  owner 'root'
  group 'root'
  mode 0o400
  notifies :restart, 'service[metricbeat]', :delayed
  variables(config: node['elastic']['metricbeat'])
end
