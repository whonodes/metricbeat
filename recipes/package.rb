apt_repository 'elastic-7.x' do
  uri 'https://artifacts.elastic.co/packages/7.x/apt'
  key 'https://artifacts.elastic.co/GPG-KEY-elasticsearch'
  components ['main']
  distribution 'stable'
  action :add
  deb_src false
  only_if node['elastic']['repo']['7.x']
end

package 'metricbeat' do
  package_name 'metricbeat'
  action node['elastic']['metricbeat']['package']
  timeout 60
  version node['elastic']['metricbeat']['version'] unless node['elastic']['metricbeat']['version'].nil?
end
