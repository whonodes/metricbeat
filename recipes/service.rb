service 'metricbeat' do
  action node['elastic']['metricbeat']['state'].to_sym
  action node['elastic']['metricbeat']['enabled'].to_sym
end
