#
# Cookbook:: metricbeat
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.
include_recipe 'metricbeat::package'
include_recipe 'metricbeat::service'
include_recipe 'metricbeat::configure'
