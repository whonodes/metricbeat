node.default['elastic']['metricbeat']['module']['system']['enabled'] = true
node.default['elastic']['metricbeat']['module']['system']['cpu'] = true
node.default['elastic']['metricbeat']['module']['system']['load'] = true
node.default['elastic']['metricbeat']['module']['system']['memory'] = true
node.default['elastic']['metricbeat']['module']['system']['network'] = true

# node.default['elastic']['metricbeat']['module']['system']['process'] = true
node.default['elastic']['metricbeat']['module']['system']['process']['include_top_n']['by_cpu'] = 10
node.default['elastic']['metricbeat']['module']['system']['process']['include_top_n']['by_memory'] = 10

node.default['elastic']['metricbeat']['module']['system']['process_summary'] = true
node.default['elastic']['metricbeat']['module']['system']['uptime'] = true
node.default['elastic']['metricbeat']['module']['system']['socket_summary'] = true
node.default['elastic']['metricbeat']['module']['system']['core'] = false
node.default['elastic']['metricbeat']['module']['system']['diskio'] = false
node.default['elastic']['metricbeat']['module']['system']['filesystem'] = false
node.default['elastic']['metricbeat']['module']['system']['fsstat'] = false
node.default['elastic']['metricbeat']['module']['system']['raid'] = false
node.default['elastic']['metricbeat']['module']['system']['socket'] = false
