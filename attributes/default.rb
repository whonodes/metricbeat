node.default['elastic']['metricbeat']['package'] = 'install'
node.default['elastic']['metricbeat']['enabled'] = 'enable'
node.default['elastic']['metricbeat']['state'] = 'start'
node.default['elastic']['metricbeat']['outputs']['elasticsearch']['enabled'] = 'false'
node.default['elastic']['metricbeat']['outputs']['elasticsearch']['hosts'] = 'localhost:9200'

node.default['elastic']['metricbeat']['logging']['level'] = 'debug'

node.default['elastic']['metricbeat']['outputs']['logstash']['enabled'] = 'true'
node.default['elastic']['metricbeat']['outputs']['logstash']['hosts'] = 'localhost:5044'
node.default['elastic']['repo']['7.x'] = 'true'
node.default['elastic']['repo']['6.x'] = 'false'
