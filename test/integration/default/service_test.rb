describe service('metricbeat') do
  it { should be_running }
  it { should be_enabled }
end
