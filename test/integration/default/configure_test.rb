describe directory '/etc/metricbeat' do
  it { should be_directory }
  it { should be_owned_by 'root' }
  it { should be_grouped_into 'root' }
  it { should be_mode 0o644 }
end

describe directory '/etc/metricbeat/modules.d' do
  it { should be_directory }
  it { should be_owned_by 'root' }
  it { should be_grouped_into 'root' }
  it { should be_mode 0o755 }
end

describe file '/etc/metricbeat/metricbeat.yml' do
  it { should be_file }
  it { should be_owned_by 'root' }
  it { should be_grouped_into 'root' }
  it { should be_mode 0o400 }
end
